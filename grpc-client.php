<?php

require 'vendor/autoload.php';

use Reverse\Request;
use Reverse\ReverseClient;

/**
 * Initializing grpc request
 */
$msgReq = new Request();
$msgReq->setMessage(isset($argv[1]) ? $argv[1] : 'Reverse');

$client = new ReverseClient('localhost:5300', [
    'credentials' => Grpc\ChannelCredentials::createInsecure(),
]);

list($response, $status) = $client->Do($msgReq)->wait();

if ($status->code != Grpc\STATUS_OK) {
    throw new Exception($status->details, $status->code);
}

echo $response->getMessage(), '\n';


