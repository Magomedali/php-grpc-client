<?php
// GENERATED CODE -- DO NOT EDIT!

namespace Reverse;

/**
 */
class ReverseClient extends \Grpc\BaseStub {

    /**
     * @param string $hostname hostname
     * @param array $opts channel options
     * @param \Grpc\Channel $channel (optional) re-use channel object
     */
    public function __construct($hostname, $opts, $channel = null) {
        parent::__construct($hostname, $opts, $channel);
    }

    /**
     * @param \Reverse\Request $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     * @return \Grpc\UnaryCall
     */
    public function Do(\Reverse\Request $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/reverse.Reverse/Do',
        $argument,
        ['\Reverse\Response', 'decode'],
        $metadata, $options);
    }

}
